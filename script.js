const botaoGerarPiada = document.querySelector("#gerarPiada");
let textoPiada = document.querySelector("#piada");

function extrairJson(resposta){
    return resposta.json();
}

function iniciarPiada(dados){
    fetch("http://api.icndb.com/jokes/random").then(extrairJson).then(preencherPiada);
}

function preencherPiada(dados){
    let piada = dados.value.joke;
    textoPiada.innerHTML =  piada;   
}

botaoGerarPiada.onclick = iniciarPiada;